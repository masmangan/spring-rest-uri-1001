package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class SomadorTest {

	@Test
	void testGetX() {
		Somador s = new Somador(10, 9);
		int expected = 19;
		int actual = s.getX();
		assertTrue(expected == actual);
	}

}

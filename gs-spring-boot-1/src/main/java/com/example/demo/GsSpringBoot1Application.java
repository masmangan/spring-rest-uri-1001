package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GsSpringBoot1Application {

	public static void main(String[] args) {
		SpringApplication.run(GsSpringBoot1Application.class, args);
	}

}

package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SomadorController {

	@GetMapping("/sum")
	public Somador somador(@RequestParam int a, @RequestParam int b) {
		Somador s = new Somador(a, b);
		return s;
	}
}

package com.example.demo;

public class Somador {

	private int a;
	private int b;
	private int soma;
	
	Somador(int a, int b) {
		super();
		this.a = a;
		this.b = b;
		soma = a + b;
	}
	
	public int getA() {
		return a;
	}

	public int getB() {
		return b;
	}

	public int getX() {
		return soma;
	}
	
}
